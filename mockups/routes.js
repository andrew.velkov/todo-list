module.exports = {
  "/api/*": "/$1",
  "/managers?search=:string": "/managers?q=:string",
  "/todo?search=:string": "/todo?q=:string",
  "/todo": "/todo",
  "/todo/*": "/todo/$1",
  "/todo?*": "/todo?$1",
}
