import { applyMiddleware, compose } from 'redux';
import { createStore } from 'redux-dynamic-reducer';
import thunk from 'redux-thunk';

import DevTools from 'utils/dev/redux-dev-tools';
import reducer from './modules/reducer';
import apiMiddleware from './middleware/apiMiddleware';

function configureStore() {
  const defaultMiddleware = [thunk, apiMiddleware];
  let middleware;
  let createStoreWithMiddleware;

  if (__IS_PRODUCTION__) {
    middleware = applyMiddleware(...defaultMiddleware);
    createStoreWithMiddleware = compose(middleware);
  } else {
    middleware = applyMiddleware(...defaultMiddleware);
    createStoreWithMiddleware = compose(middleware, DevTools.instrument());
  }

  const store = createStore(reducer, createStoreWithMiddleware);

  if (!__IS_PRODUCTION__ && module.hot) {
    module.hot.accept('./modules/reducer', () => {
      store.replaceReducer(require('./modules/reducer')); // eslint-disable-line global-require
    });
  }

  return store;
}

export default configureStore();
