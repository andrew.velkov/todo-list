import { SubmissionError } from 'redux-form';
import { dispatchSuccess, dispatchErrorRequest } from 'components/Notifications/dispatcher';
import history from 'routes/history';

const CREATE = 'project/todo/CREATE';
const CREATE_SUCCESS = 'project/todo/CREATE_SUCCESS';
const CREATE_ERROR = 'project/todo/CREATE_ERROR';

const GET = 'project/todo/GET';
const GET_SUCCESS = 'project/todo/GET_SUCCESS';
const GET_ERROR = 'project/todo/GET_ERROR';

const REMOVE = 'project/todo/REMOVE';
const REMOVE_SUCCESS = 'project/todo/REMOVE_SUCCESS';
const REMOVE_ERROR = 'project/todo/REMOVE_ERROR';

const initialState = {
  create: {
    loading: false,
    loaded: false,
    status: '',
  },
  remove: {
    loading: false,
    loaded: false,
    status: '',
  },
  get: {
    loading: false,
    loaded: false,
    data: [],
  },
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE:
      return {
        ...state,
        create: {
          ...state.create,
          loading: true,
          loaded: false,
          status: '',
        },
      };
    case CREATE_SUCCESS:
      return {
        ...state,
        create: {
          ...state.create,
          loading: false,
          loaded: true,
          status: 'success',
        },
      };
    case CREATE_ERROR:
      return {
        ...state,
        create: {
          ...state.create,
          loading: false,
          status: 'error',
        },
      };
    case REMOVE:
      return {
        ...state,
        remove: {
          ...state.remove,
          loading: false,
          loaded: true,
          status: '',
        },
      };
    case REMOVE_SUCCESS:
      return {
        ...state,
        remove: {
          ...state.remove,
          loading: true,
          loaded: false,
          status: 'success',
        },
      };
    case REMOVE_ERROR:
      return {
        ...state,
        remove: {
          ...state.remove,
          loading: false,
          status: 'error',
        },
      };
    case GET:
      return {
        ...state,
        get: {
          ...state.get,
          loading: true,
          loaded: false,
        },
      };
    case GET_SUCCESS:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: true,
          data: action.result,
        },
      };
    case GET_ERROR:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: false,
        },
      };
    default:
      return state;
  }
}

export function create(data) {
  return {
    types: [CREATE, CREATE_SUCCESS, CREATE_ERROR],
    url: '/api/todo',
    method: 'POST',
    body: data,
    onSuccess: () => {
      dispatchSuccess('Todo created');
      setTimeout(() => history.push('/articles'), 1000);
    },
    onError: (res) => {
      dispatchErrorRequest(res);
      throw new SubmissionError({ _error: 'Error' });
    },
  };
}

export function remove(data) {
  return {
    types: [REMOVE, REMOVE_SUCCESS, REMOVE_ERROR],
    url: `/api/todo/${ data }`,
    method: 'DELETE',
    onSuccess: () => {
      dispatchSuccess('Todo Deleted');
      setTimeout(() => history.push('/articles'), 1000);
    },
    onError: (res) => {
      dispatchErrorRequest(res);
      throw new SubmissionError({ _error: 'Error' });
    },
  };
}

export function get() {
  return {
    types: [GET, GET_SUCCESS, GET_ERROR],
    url: '/api/todo',
    method: 'GET',
  };
}
