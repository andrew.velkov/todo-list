import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import account from './account';
import toolbar from './toolbar';
import notifications from './notifications';
import todo from './todo';

const appReducer = combineReducers({
  form: formReducer,
  account,
  notifications,
  toolbar,
  todo,
});

const rootReducer = (state, action) => {
  if (action.type === 'video/account/LOGOUT') {
    return appReducer(1);
  }
  return appReducer(state, action);
};

export default rootReducer;
