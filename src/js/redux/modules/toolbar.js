import { xor as toggle, without, uniq } from 'lodash';
import storage from 'store';

const TOGGLE = 'video/toolbar/TOGGLE';
const TOGGLE_PIN = 'video/toolbar/TOGGLE_PIN';
const CLOSE = 'video/toolbar/CLOSE';
const CLEAR = 'video/toolbar/CLEAR';

const visibility = ['filters'];

const initialState = {
  visibility: uniq([...visibility, ...storage.get('video-toolbar-pin') || []]),
  pin: storage.get('video-toolbar-pin') || [],
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case TOGGLE:
      return {
        ...state,
        visibility: state.pin.includes(action.data) ? state.visibility : toggle(state.visibility, [action.data]),
      };
    case TOGGLE_PIN: {
      const toggledPin = toggle(state.pin, [action.data]);
      storage.set('video-toolbar-pin', toggledPin);
      return {
        ...state,
        pin: toggledPin,
      };
    }
    case CLOSE:
      return {
        ...state,
        visibility: toggle(state.visibility, without(state.visibility, ...state.pin)),
      };
    case CLEAR:
      return {
        ...initialState,
        visibility: uniq([...visibility, ...storage.get('video-toolbar-pin') || []]),
        pin: storage.get('video-toolbar-pin') || [],
      };
    default:
      return state;
  }
}

export function toggleVisibility(data) {
  return {
    type: TOGGLE,
    data,
  };
}

export function togglePin(data) {
  return {
    type: TOGGLE_PIN,
    data,
  };
}

export function close() {
  return {
    type: CLOSE,
  };
}

export function clear() {
  return {
    type: CLEAR,
  };
}
