import { logout, expiredSession, LOGIN_ERROR } from 'redux/modules/account';

const headers = {
  JSON: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  FILE: {
    'Accept': 'application/octet-stream',
  },
};

const apiMiddleware = (store) => (next) => (action) => {
  const { url, types, method = 'GET', header = 'JSON', body, onSuccess, onError, ...rest } = action;
  const { dispatch } = store;
  if (!types) {
    return next(action);
  }
  const [REQUEST, SUCCESS, FAILURE] = types;

  next({ ...rest, type: REQUEST });

  return fetch(encodeURI(url), {
    method,
    credentials: 'same-origin',
    headers: headers[header],
    body: header === 'JSON' ? JSON.stringify(body) : body,
  }).then((res) => {
    if (res.status >= 200 && res.status < 300) {
      return res.json();
    }

    if (res.status === 401 || REQUEST === 'panel/account/LOAD_PROFILE') {
      const isExpiredSession = store.getState().account.profile.loaded;
      if (isExpiredSession) {
        dispatch(expiredSession());
      }

      next({ type: FAILURE });
      if (FAILURE !== LOGIN_ERROR) {
        dispatch(logout());
      }
    }

    return res.json();
  }).then(({ result, ...restData }) => {
    if ((restData.status === 'success' && restData.success.code === 200)) {
      let resultProcessed;

      if (onSuccess) {
        resultProcessed = onSuccess(result, ...restData);
      }
      next({ response: restData, result: resultProcessed || result, type: SUCCESS });
      return Promise.resolve({ ...rest, result });
    }

    if (onError) {
      onError(restData, action);
    }
    next({ ...rest, result: restData.error, type: FAILURE });

    return Promise.reject(new Error('Error request'));
  });
};

export default apiMiddleware;
