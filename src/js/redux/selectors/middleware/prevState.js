import { isEqual } from 'lodash';

export default function prevStateMiddleware(next, computeResult) {
  let prevResult = null;
  let prevArguments = null;

  return function (...rest) {
    let value;

    if (isEqual(rest, prevArguments) && prevResult !== null) {
      value = prevResult;
    } else {
      const result = next(...rest);
      value = computeResult ? computeResult(result, prevResult) : result;
      prevResult = result;
    }

    prevArguments = rest;
    return value;
  };
}
