exports.HOST = {
  production: 'todo-list.ho.ua',
  development: 'todo-list.ho.ua',
  stage: '',
  local: 'localhost:3003',
};

exports.DEVELOPMENT = {
  enableAnalyticsKey: 'enable-analytics-key',
  enableAnalyticsLog: 'enable-analytics-log',
};
