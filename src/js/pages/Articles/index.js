import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import { get as getTodoList, create as createTodoList, remove as removeTodoList } from 'redux/modules/todo';
import Header from 'components/Header';
import Button from 'components/Button';
import Fetching from 'components/Fetching';
import Field from 'components/Form/Elements/Input';
import Select from 'components/Form/Elements/Select';

import css from 'style/pages/Articles';

const sortItems = [
  { id: 0, value: 'Sort By' },
  { id: 1, value: 'Date' },
  { id: 2, value: 'Views' },
];

@connect((state) => ({
  todoList: state.todo.get,
}),
{ getTodoList, createTodoList, removeTodoList }
)
export default class Articles extends Component {
  static propTypes = {
    todoList: PropTypes.object,
    createTodoList: PropTypes.func,
    removeTodoList: PropTypes.func,
    getTodoList: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: '',
      todoTitle: '',
      todoDesc: '',
      sort: 0,
    };
  }

  componentDidMount() {
    this.props.getTodoList();
  }

  searchingArticles = (value) => (data) => data.todo_title.toLowerCase().includes(value.toLowerCase()) || data.todo_desc.toLowerCase().includes(value.toLowerCase()) || !value;

  sortArticles = (event, index) => {
    this.setState({
      sort: index,
    });
  }

  sorts = (a, b) => {
    if (this.state.sort === 1) {
      const c = new Date(a.todo_date);
      const d = new Date(b.todo_date);

      return c > d ? -1 : 1;
    } else if (this.state.sort === 2) {
      const c = new Date(a.todo_views);
      const d = new Date(b.todo_views);

      return c > d ? -1 : 1;
    }

    return 0;
  }

  handleChangeSearch = (event) => {
    this.setState({
      value: event.target.value,
    });
  };

  handleChangeTitle = (event) => {
    this.setState({
      todoTitle: event.target.value,
    });
  };

  handleChangeDesc = (event) => {
    this.setState({
      todoDesc: event.target.value,
    });
  };

  handleChangeUrl = (event) => {
    this.setState({
      todoUrl: event.target.value,
    });
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    const date = new Date();
    const dateArticle = {
      dates: date.getDate(),
      month: date.getMonth(),
      year: date.getFullYear(),
      result() {
        return `${ this.dates }/${ this.month }/${ this.year }`;
      },
    };

    if (this.state.todoTitle && this.state.todoDesc) {
      await this.props.createTodoList({
        todo_title: this.state.todoTitle,
        todo_desc: this.state.todoDesc,
        todo_url: this.state.todoUrl,
        todo_views: Math.floor(Math.random() * 100) || 5,
        todo_date: dateArticle.result(),
      });

      this.props.getTodoList();
    }

    this.setState({
      todoTitle: '',
      todoDesc: '',
      todoUrl: '',
    });
  }

  handleDelete = async (id) => {
    await this.props.removeTodoList(id);
    this.props.getTodoList();
  }

  render() {
    const { loading, loaded, data } = this.props.todoList;
    const { todoTitle, todoDesc, todoUrl, value, sort } = this.state;

    return (
      <section className={ cx(css.section, css.section_gray) }>
        <Header title='Articles' />
        <section className={ cx(css.wrap, css.wrap_medium) }>
          <ul className={ css.grid }>

            <li className={ cx(css.grid__item, css.grid__item_8) }>
              <div className={ css.grid__ibox }>
                <h3 className={ css.grid__title }>Add Articles</h3>
                <form onSubmit={ this.handleSubmit }>
                  <div className={ cx(css.formGroup, css.grid) }>
                    <div className={ css.grid__item }>
                      <Field label='Title *' value={ todoTitle } onChange={ this.handleChangeTitle } />
                    </div>
                    <div className={ css.grid__item }>
                      <Field label='Description *' value={ todoDesc } onChange={ this.handleChangeDesc } multiLine={ true } />
                    </div>
                    <div className={ css.grid__item }>
                      <Field label='Url image' value={ todoUrl } onChange={ this.handleChangeUrl } />
                    </div>
                  </div>
                  <Button type='submit' primaryClass={ true } disabled={ todoDesc === '' || todoTitle === '' }>Add Article</Button>
                </form>
              </div>
            </li>

            <li className={ cx(css.grid__item, css.grid__item_4) }>
              <div className={ css.grid__ibox }>
                <h3 className={ css.grid__title }>Search</h3>
                <Field label='Search...' value={ value } onChange={ this.handleChangeSearch } />
              </div>
            </li>

            <li className={ css.grid__item }>
              <div className={ css.grid__ibox }>
                <h3 className={ cx(css.grid__title, css.grid__title_between) }>
                  <span>Todo List</span>
                  <Select className={ css.todoList__sort } label='' data={ sortItems } value={ sort } handleChange={ this.sortArticles } />
                </h3>
                <div>
                  <Fetching isFetching={ loading } size={ 45 } thickness={ 8 } position='initial' />

                  {loaded && <ul className={ css.todoList }>
                    {data.filter(this.searchingArticles(value)).reverse().sort(this.sorts).map((item, index) => {
                      return (
                        <li key={ item.id } className={ css.todoList__item }>
                          <h4 className={ css.todoList__title }>
                            <span>{ index + 1 }. { item.todo_title }</span>
                            <span className={ css.todoList__date }>views: { item.todo_views } - { item.todo_date }</span>
                          </h4>
                          <p>
                            { item.todo_desc }
                          </p>
                          <div className={ css.buttonGroup }>
                            <div className={ css.buttonGroup__item }>
                              <Button typeButton='link' to={ `/articles/${ item.id }` } primary={ true }>Details...</Button>
                            </div>
                            <div className={ css.buttonGroup__item }>
                              <Button typeButton='raise' iconName='close' onClick={ () => this.handleDelete(item.id) } secondary={ true }>Delete Article</Button>
                            </div>
                          </div>
                        </li>
                      );
                    })}

                    {data.filter(this.searchingArticles(value)).length <= 0 && <div className={ css.well }>
                      <p>No articles...</p>
                    </div>}
                  </ul>}
                </div>
              </div>
            </li>
          </ul>
        </section>
      </section>
    );
  }
}
