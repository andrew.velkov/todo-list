import React, { Component } from 'react';
import cx from 'classnames';

import Checkbox from 'components/Form/Elements/Checkbox';
import CheckboxButton from 'components/Form/Elements/CheckboxButton';
import RadioButton from 'components/Form/Elements/RadioButton';
import Input from 'components/Form/Elements/Input';

import css from 'style/pages/Dashboard';

export default class Dashboard extends Component {
  render() {
    return (
      <div>
        <div className={ cx(css.formGroup, css.formGroup_reset) }>
          <Input floatingLabelText='floatingLabelText' />
          <Input floatingLabelText='floatingLabelText' />
        </div>

        <div className={ css.formGroup }>
          <h4>Checkbox</h4>
          <ul className={ css.checkboxGroup }>
            <li className={ css.checkboxGroup__item }>
              <Checkbox label='label' />
            </li>
            <li className={ css.checkboxGroup__item }>
              <Checkbox label='labelф asas' />
            </li>
            <li className={ css.checkboxGroup__item }>
              <Checkbox label='label фыв фы' />
            </li>
            <li className={ css.checkboxGroup__item }>
              <Checkbox label='label' />
            </li>
            <li className={ css.checkboxGroup__item }>
              <Checkbox label='label' />
            </li>
            <li className={ css.checkboxGroup__item }>
              <Checkbox label='label' />
            </li>
          </ul>
        </div>

        <div className={ css.formGroup }>
          <h4>CheckboxButton</h4>
          <div className={ css.formGroup__wrap }>
            <div className={ css.checkboxButtonGroup }>
              <CheckboxButton name='checkbox_name' label='Checkbox label das' value='1' />
              <CheckboxButton name='checkbox_name' label='Checkbox label' value='2' />
              <CheckboxButton name='checkbox_name' label='Checkbox' value='2' />
              <CheckboxButton name='checkbox_name' label='Checkbox label' value='2' />
              <CheckboxButton name='checkbox_name' label='Checkbox' value='2' />
              <CheckboxButton name='checkbox_name' label='Checkbox label' value='2' />
              <CheckboxButton name='checkbox_name' label='Checkbox' value='2' />
              <CheckboxButton name='checkbox_name' label='Checkbox' value='2' />
            </div>
          </div>
        </div>

        <div className={ cx(css.formGroup, css.formGroup_reset) }>
          <Input floatingLabelText='floatingLabelText' />
        </div>

        <div className={ css.formGroup }>
          <h4>RadioButton</h4>
          <div className={ css.formGroup__wrap }>
            <div className={ css.checkboxButtonGroup }>
              <RadioButton name='checkbox_name' label='Checkbox label das' value='1' />
              <RadioButton name='checkbox_name' label='Checkbox label' value='2' />
              <RadioButton name='checkbox_name' label='Checkbox' value='3' />
              <RadioButton name='checkbox_name' label='Checkbox label' value='4' />
              <RadioButton name='checkbox_name' label='Checkbox' value='5' />
            </div>
          </div>
        </div>

        <div className={ cx(css.formGroup, css.formGroup_reset) }>
          <Input floatingLabelText='floatingLabelText' />
        </div>
      </div>
    );
  }
}
