import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as getSupply from 'redux/modules/supplies';
import Select from './Select';

@connect((state, props) => ({
  supply: state.supplies[props.options.supply],
}),
(dispatch) => ({
  getSupply: bindActionCreators(getSupply, dispatch),
}))
export default class SelectSupply extends Component {
  static propTypes = {
    label: PropTypes.string,
    input: PropTypes.object,
    supply: PropTypes.object,
    getSupply: PropTypes.object,
    options: PropTypes.object,
    onChange: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.getSupply = props.getSupply[this.props.options.supply];
  }

  state = {
    value: null,
  };

  componentDidMount() {
    this.getSupply();
  }

  componentWillReceiveProps(nextProps) {
    const { input: { value } } = nextProps;
    if (value !== this.props.input.value) {
      this.setState({
        value,
      });
    }
  }

  handleChange = async (event, index, value) => {
    await this.setState({ value });
    this.props.input.onChange(value);
  };

  render() {
    const { label, supply: { data } } = this.props;

    return (
      <Select
        label={ label }
        data={ data }
        value={ this.state.value }
        handleChange={ this.handleChange }
      />
    );
  }
}
