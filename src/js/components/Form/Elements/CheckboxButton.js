import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import Button from 'components/Button';

import css from 'style/components/CheckboxButton';

const CheckboxButton = ({ label, input: { name }, fullWidth, options: { data }, onChange, values }) => (
  <div className={ css.formGroup }>
    <h4>{ label }</h4>
    <div className={ css.formGroup__wrap }>
      <div className={ css.checkboxButton__group }>
        {data.map((item) => {
          return (
            <div key={ item.id } className={ cx(css.checkboxButton, { [css.checkboxButton_full]: fullWidth }) }>
              <input
                className={ css.checkboxButton__input }
                id={ `${ name }_${ item.id }` }
                type='checkbox'
                name={ name }
                value={ item.id }
                onChange={ onChange }
                checked={ values && values.includes(item.id) }
              />
              <Button
                className={ css.checkboxButton__label }
                label={ item.value }
                htmlFor={ `${ name }_${ item.id }` }
                typeButton='flat'
                containerElement='label'
              />
            </div>
          );
        })}
      </div>
    </div>
  </div>
);

CheckboxButton.propTypes = {
  label: PropTypes.string,
  fullWidth: PropTypes.bool,
  values: PropTypes.array,
  options: PropTypes.object,
  input: PropTypes.object,
  onChange: PropTypes.func,
};

export default CheckboxButton;
