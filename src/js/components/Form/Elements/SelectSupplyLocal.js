import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Select from './Select';

@connect((state, props) => ({
  supply: state.supplies[props.options.supply],
}))
export default class SelectSupplyLocal extends Component {
  static propTypes = {
    label: PropTypes.string,
    input: PropTypes.object,
    supply: PropTypes.object,
    onChange: PropTypes.func,
  };

  state = {
    value: null,
  };

  componentWillReceiveProps(nextProps) {
    const { input: { value } } = nextProps;
    if (value !== this.props.input.value) {
      this.setState({
        value,
      });
    }
  }

  handleChange = async (event, index, value) => {
    await this.setState({ value });
    this.props.input.onChange(value);
  };

  render() {
    const { label, supply: { data } } = this.props;

    return (
      <Select
        label={ label }
        data={ data }
        value={ this.state.value }
        handleChange={ this.handleChange }
      />
    );
  }
}
