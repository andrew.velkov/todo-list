import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import RadioButton from './RadioButton';

@connect((state, props) => ({
  supply: state.supplies[props.options.supply],
}))
export default class RadioButtonSupply extends Component {
  static propTypes = {
    disabled: PropTypes.bool,
    input: PropTypes.object,
    supply: PropTypes.object,
    onChange: PropTypes.func,
  };

  state = {
    value: null,
  };

  componentWillReceiveProps(nextProps) {
    const { input: { value } } = nextProps;
    this.setState({
      value,
    });
  }

  handleChange = (event) => {
    this.setState({
      value: +event.target.value,
    });
    this.props.input.onChange(+event.target.value);
  };

  render() {
    const { value } = this.state;
    const { supply: { data }, disabled } = this.props;
    return (
      <RadioButton { ...this.props } options={ { data } } onChange={ this.handleChange } value={ value } disabled={ disabled } />
    );
  }
}
