import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox as CheckboxMaterial } from 'material-ui';

this.styles = {
  fontSize: 14,
  padding: '0.5rem 0',
};

const Checkbox = ({ label, onChange, name, checked, ...props }) => (
  <CheckboxMaterial style={ this.styles } label={ label } name={ name } checked={ checked } onCheck={ onChange } { ...props } />
);

Checkbox.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
};

export default Checkbox;
