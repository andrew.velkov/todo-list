const reduxForm = require('redux-form');
const formElement = require('./Elements');
const formValidate = require('./validate');

module.exports = {
  ...reduxForm,
  formElement,
  formValidate,
};
