# todo-list

```
$ yarn install
```

```
$ npm install -g yarn
```

Visit `http://localhost:3003/` from your browser of choice (inline mode).<br />
In iframe mode just navigate the browser to `http://localhost:3003/webpack-dev-server/`<br />

For start server with mockups:

```
$ yarn start:mock
```

Build will be placed in the `build` folder.

```
$ yarn build
```